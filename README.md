# astra-eis-hello

This is a minimal implementation of an Environment Interface Standard (EIS) project
that is configured for [HelloWorldEnvironment](https://github.com/eishub/HelloWorldEnvironment).

To run this project, you must have [Maven 3.3+](http://apache.maven.org) installed.

Simply clone the project, go to the project root folder and type mvn to run the
project.